# Guidelines

As there will be several people contributing to this Brainfuck interpreter and compiler, it is important to follow the rules already followed in the existing code.
The most basic rules are:
* Follow the same rules as everyone else
* Have a readable and maintainable code
* Publish code that works
* Do not create memory leaks

## Coding Style

This project follows a coding style similar to Google's C++ Style Guide for C source code. The C code follows the C99 standard. The compilers this project is compiled with are gcc/g++ 7.x and clang/clang++ 4.x
The Python code follows a similar style to Google's Python Style Guide. You can find Google's guidelines at this address: https://google.github.io/styleguide/pyguide.html
The Python code is bridged to the C code with Cython.

The assembly code is written in the x86 AT&T dialect and has been only compiled and tested under Arch Linux. Try to compile it on other GNU/Linux distribution or OS at your own risks.

## Some coding reminders

* **Check** and **test** your code! Do not hesitate to often compile your project to test if you add any new function or you modify one. Everyone can potentially write the biggest mistake possible, no matter how experienced the programmer is.

* *Write safe code!* Because all of the C, C++ and especially the Assembly languages allow their user to modify at will the memory, we are not safe from potential memory leaks and disastrous consequences, hence why if it is safer to write thrice as much code to make some lines safer, it is often better to do so.

* *Making it work before making it fast.* Optimisation before completing first the main objectives can possibly be the worst idea ever, and it leads to hard to read and maintain code and programs. First make sure everything works. Then maybe you can try to optimize the memory usage and the speed of the software.


This file might be updated in the future, but what has been said in this version is most likely to stay true later. If you have any question, feel free to ask. Welcome aboard!

– @Phundrak
