# Brainfucker: a Brainfuck Interpreter and Compiler

This project is a quick two-day long project between two friends trying to create a software that can work both as an interpreter and as a compiler for the Brainfuck language. You can fide more information on the Brainfuck language at:
https://esolangs.org/wiki/Brainfuck

The aim is to create:
* a cli interface in which the user can enter brainfuck code, and when the enter key is pressed, the code is interpreted, and its output is displayed
* a compiler which turns an input file into an executable file through the creation of an assembly file in the Intel x86 NASM dialect

Written in C99, C++14 and Intel x86 NASM Assembly, tested only under up-to-date stable Arch Linux installations (July 2017). Compile on other systems *at your own risks*!

## Dependencies:
* gcc >= 7.0

or
* clang >= 4.0
 
* CMake >= 3.8

## Code style
See CONTRIBUTING.md

## Contributing
See CONTRIBUTING.md

### Reminder:
This is only a small project between two friends. The main aim is for both of us to have fun.