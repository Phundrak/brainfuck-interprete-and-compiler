  .file "compile.s"

  .globl  compile
  .type compile, @function
compile:
  pushq %rbp
  movq  %rsp, %rbp
  subq  $4144, %rsp

  ##    -8(%rbp) : char* inname
  ##   -16(%rbp) : char* outname
  ##   -24(%rbp) : FILE* infile
  ##   -32(%rbp) : FILE* outfile
  ##   -36(%rbp) : [ counter
  ##   -40(%rbp) : ] counter
  ##   -44(%rbp) : loop ID index
  ##   -48(%rbp) : loop counter
  ## -4144(%rbp) : int[1024], loop ID array
  ##
  ##  + = 43
  ##  , = 44
  ##  - = 45
  ##  . = 46
  ##  < = 60
  ##  > = 62
  ##  [ = 91
  ##  ] = 93
  ##
  ##     -4(%rbp) : index
  ##  -4100(%rbp) : int[1024]
  ##

  movq  %rdi, -8(%rbp)
  movq  %rsi, -16(%rbp)

  ## initialize the array at 0
  movl  $0, %edi
.cleararraybeg:
  cmpl  $1024, %edi
  je  .cleararrayend
  movq  $0, -4144(%rbp, %rdi, 4)
  addq  $1, %rdi
  jmp .cleararraybeg
.cleararrayend:
  movq  $0, -24(%rbp)
  movq  $0, -32(%rbp)
  movq  $0, -36(%rbp)
  movq  $0, -40(%rbp)
  movq  $0, -44(%rbp)
  movq  $0, -48(%rbp)

## open the files
  ## open the source code
  leaq  .strread(%rip), %rsi
  movq  -8(%rbp), %rdi
  call  fopen@PLT
  cmpl  $0, %eax
  je  .errorioin
  movq  %rax, -24(%rbp) ## *infile moved to -24(%rbp)

  ## open the output file
  leaq  .strwrite(%rip), %rsi
  movq  -16(%rbp), %rdi
  call  fopen@PLT
  cmpl  $0, %eax
  je  .errorioout
  movq  %rax, -32(%rbp) ## *outfile moved to -36(%rbp)

  movq  -32(%rbp), %rdi
  leaq  .strasminit(%rip), %rsi
  movq  -16(%rbp), %rdx
  call  fprintf@PLT

	####################################################
	## DO STUFF WITH THE FILES HERE
	####################################################

.L0:
  movq  -24(%rbp), %rdi
  call  fgetc@PLT
  movb  %al, %cl
  cmpb  $-1, %cl                ## if fgetc(*infile) == EOF
  je  .endreading
  cmpb  $43, %cl                ## if fgetc(*infile) == '+'
  je  .Lplus
  cmpb  $45, %cl                ## if fgetc(*infile) == '-'
  je  .Lminus
  cmpb  $60, %cl                ## if fgetc(*infile) == '<'
  je  .Llt
  cmpb  $62, %cl                ## if fgetc(*infile) == '>'
  je  .Lgt
  cmpb  $46, %cl                ## if fgetc(*infile) == '.'
  je  .Lpoint
  cmpb  $44, %cl                ## if fgetc(*infile) == ','
  je  .Lcomma
  cmpb  $91, %cl
  je  .Lobracket
  cmpb  $93, %cl
  je .Lcbracket


  jmp .L0
.Lplus:
  leaq  .strplus(%rip), %rsi
  jmp .Lwrite
.Lminus:
  leaq  .strminus(%rip), %rsi
  jmp .Lwrite
.Llt:
  leaq  .strlt(%rip), %rsi
  jmp .Lwrite
.Lgt:
  leaq  .strgt(%rip), %rsi
  jmp .Lwrite
.Lpoint:
  leaq  .strpoint(%rip), %rsi
  jmp .Lwrite
.Lcomma:
  leaq  .strcomma(%rip), %rsi
  jmp .Lwrite
.Lobracket:
  movq  -44(%rbp), %rax
  addq  $1, %rax                ## loop[++i] / index++
  movq  %rax, -44(%rbp)
  movq  -48(%rbp), %rax
  addq  $1, %rax                ## loopcounter++
  movq  %rax, -48(%rbp)
  leaq  .strobracket(%rip), %rsi
  movq  -48(%rbp), %rdx         ## write ".LC'loopcounter':\n"
  jmp .Lwrite
.Lcbracket:
  movq  -44(%rbp), %rax
  subq  $1, %rax                ## loop[--i] / index--
  movq  %rax, -44(%rbp)
  cmpl  $0, %eax
  jl  .toomanycbracket
  leaq  .strcbracket(%rip), %rsi
  movq  -48(%rbp), %rdx
.Lwrite:
  movq  -32(%rbp), %rdi
  movq  $0, %rax
  callq fprintf@PLT
  jmp .L0

	####################################################
	## END STUFF HERE
	####################################################

.endreading:
  movq  -32(%rbp), %rdi
  leaq  .strasmend(%rip), %rsi
  call  fprintf@PLT

  cmpq  $0, -24(%rbp)
  je .closeout          ## if *infile != NULL jump
	movq  -24(%rbp), %rdi ## close *infile
	call  fclose@PLT
.closeout:
	cmpq  $0, -32(%rbp)   ## if *infile == NULL jump
	je  .endcompile
	movq  -32(%rbp), %rdi ## close *outfile
	call  fclose@PLT
.endcompile:
  movl  $0, %eax
.end:
  nop
  leave
  ret
.errorioin:
  movq  -8(%rbp), %rsi
  jmp .errorio
.errorioout:
  movq  -16(%rbp), %rsi
.errorio:
  leaq  .strerrorio(%rip), %rdi
  call  printf@PLT
  movl  $1, %eax
  jmp .end
.toomanycbracket:
  leaq  .strtoomanycbrackets(%rip), %rdi
  movq  $0, %rax
  call  printf@PLT
  movq  $1, %rax
  jmp .end

#################### STRINGS ##################

.strasminit:
  .string ".file \"%s\"\n.globl main\n.type main, @function\nmain:\npushq %%rbp\nmovq %%rsp,%%rbp\nsubq $4100,%%rsp\nmovq $0,-4(%%rbp)\nmovq $-1,%%rax\nmovq %%rax,-44(%%rbp)\nmovq %%rax,-48(%%rbp)\n"
.strread:
  .string "r"
.strwrite:
  .string "w"
.strobracket:
  .string "## [\n.LC%d:\n"
.strcbracket:
  .string "## ]\nmovq -4(%%rbp),%%rsi\nmovq -4100(%%rbp,%%rsi,4),%%rax\ncmpl $0,%%eax\njne .LC%d\n"
.strerrorio:
  .string "Error: could not open %s.\n"
.strplus:
  .string "## +\nmovq -4(%%rbp),%%rdi\nmovq -4100(%%rbp,%%rdi,4), %%rsi\naddq $1,%%rsi\nmovq %%rsi,-4100(%%rbp,%%rdi,4)\nmovq %%rdi,-4(%%rbp)\ncmpl $0,%%esi\njl .minreached\ncmpl $32766,%%esi\njg .maxreached\n"
.strminus:
  .string "## -\nmovq -4(%%rbp),%%rdi\nmovq -4100(%%rbp,%%rdi,4), %%rsi\nsubq $1,%%rsi\nmovq %%rsi,-4100(%%rbp,%%rdi,4)\nmovq %%rdi,-4(%%rbp)\ncmpl $0,%%esi\njl .minreached\ncmpl $32766,%%esi\njg .maxreached\n"
.strlt:
  .string "## <\nmovq -4(%%rbp),%%rdi\nsubq $1,%%rdi\nmovq %%rdi,-4(%%rbp)\ncmpq $0,%%rdi\njl .indexminreached\ncmpq $1022,%%rdi\njg .indexmaxreached\n"
.strgt:
  .string "## >\nmovq -4(%%rbp),%%rdi\naddq $1,%%rdi\nmovq %%rdi,-4(%%rbp)\ncmpq $0,%%rdi\njl .indexminreached\ncmpq $1022,%%rdi\njg .indexmaxreached\n"
.strpoint:
  .string "## .\nmovq -4(%%rbp),%%rcx\nmovq -4100(%%rbp,%%rcx,4),%%rsi\nleaq .strchar(%%rip),%%rdi\nmovq $0,%%rax\ncall printf@PLT\n"
.strcomma:
  .string "## ,\ncall getchar@PLT\nmovq -4(%%rbp),%%rdi\nmovq %%rax,-4100(%%rbp,%%rdi,4)\n"
.strasmend:
	.string "movl $0,%%eax\n.end:\nleaq .strendl(%%rip),%%rdi\nmovq $0,%%rax\ncall printf@PLT\nmovl $0,%%eax\nleave\nret\n.minreached:\nmovq -4(%%rbp),%%rsi\nmovq -4100(%%rbp,%%rsi,4),%%rdx\nleaq .strmin(%%rip),%%rdi\nmovq $0,%%rax\ncall printf@PLT\njmp .end\n.maxreached:\nmovq -4(%%rbp),%%rsi\nmovq -4100(%%rbp,%%rsi,4),%%rdx\nleaq .strmax(%%rip),%%rdi\nmovq $0,%%rax\ncall printf@PLT\njmp .end\n.indexminreached:\nmovq %%rdi,%%rsi\nleaq .strindexmin(%%rip),%%rdi\nmovq $0,%%rax\ncall printf@PLT\njmp end\n.indexmaxreached:\nmovq %%rdi,%%rsi\nleaq .strindexmax(%%rip),%%rdi\nmovq $0,%%rax\ncall printf@PLT\njmp .end\n.strmax:\n.string \"Error: maximal value reached at index %%x: %%x\\n\"\n.strmin:\n.string \"Error: negative value reached at index %%d: %%d\\n\"\n.strindexmin:\n.string \"Error: index reached negative value: %%d\\n\"\n.strindexmax:\n.string \"Error: index reached maximal value: %%d\\n\"\n	.strchar:\n.string \"%%c\"\n.strendl:\n.string \"\\n\"\n"
.strtoomanycbrackets:
  .string "You have too many closing brackets. Please verify your code, then try again.\n"
