#include <stdio.h>
#include <string.h>

#include <Python.h>

#define DEBUG if(1) printf

int compile(char *, char *); // input file, output file

int error_args(int return_code) {
  fprintf(stderr, "Incorrect arguments passed. Please use the interpreter/compiler\n"
          "as follows:\n\n"
          "brainfucker -i inputfile\n\tlaunch the interpreter with the\n"
          "\tcorresponding input file\n\n"
          "brainfucker -c inputfile [-o outputfile]\n\tlaunch the compiler\n"
          "\twith the corresponding input file. If an output filename is not\n"
          "\tspecified, the default name will be \"output.s\"\n\n");
  return return_code;
}

int call_interpreter(wchar_t *program_name, char *input_file) {
  Py_SetProgramName(program_name);
  Py_Initialize();

  PyRun_SimpleString("print('Hello world!')");
}

int main(int argc, char *argv[]) {
  wchar_t *program = Py_DecodeLocale(argv[0], NULL);
  if (program == NULL) {
    fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
  }
  if (argc < 3){
    return error_args(1);
  }

  for(int i = 0; i < argc; ++i) {
    printf("argv[%d] = \"%s\"\n", i, argv[i]);
  }

  if(strcmp(argv[1], "-i") == 0) {
    if(!argv[2])
      call_interpreter(program, NULL);
    else
      call_interpreter(program, argv[2]);
  } else if(strcmp(argv[1], "-c") == 0) {
    // launch the compiler
    // the file is stored in argv[2]
    char* infile = argv[2];
    printf("infile = %s\n", infile);
    char* outfile = NULL;

    if(argc > 3){
      if(strcmp(argv[3], "-o") == 0){
        printf("Output file specified\n");
        if(argv[4] == NULL)
          return error_args(1);
        outfile = argv[4];
      } else {
        outfile = "output.s";
      }
    } else {
      outfile = "output.s";
    }

    printf("outfile = %s\n", outfile);

    return compile(infile, outfile);
  }

  return 0;
}
